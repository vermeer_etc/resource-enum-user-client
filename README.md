annotation-processor-resource-enum の利用サンプルで生成したクラスを利用するサンプル
===

## Licence

Licensed under the [Apache License, Version 2.0](http://www.apache.org/licenses/LICENSE-2.0)

## Author

[_vermeer_](https://twitter.com/_vermeer_)